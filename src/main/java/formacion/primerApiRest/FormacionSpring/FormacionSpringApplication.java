package formacion.primerApiRest.FormacionSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormacionSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormacionSpringApplication.class, args);
	}

}
